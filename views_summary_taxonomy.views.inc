<?php
/**
 * @file
 * Include file of views_summary_taxonomy.
 */

/**
 * Implements of hook_views_plugins().
 */
function views_summary_taxonomy_views_plugins() {
  return array(
    'style' => array(
      'taxonomy_summary' => array(
        'title' => t('Taxonomy'),
        'help' => t('Displays the summary as a taxonomy tree.'),
        'handler' => 'views_summary_taxonomy_plugin_style',
        'parent' => 'default_summary',
        'type' => 'summary',
        'uses options' => TRUE,
      ),
    ),
  );
}

/**
 * Implements of hook_views_data_alter().
 */
function views_summary_taxonomy_views_data_alter(&$data) {
  $data['term_node']['term_node_tid_depth_summary'] = array(
    'group' => t('Taxonomy'),
    'title' => t('Term ID (with depth and summary)'),
    'help' => t('Taxonomy term argument that supports depth and summary view.'),
    'real field' => 'tid',
    'argument' => array(
      'handler' => 'views_summary_taxonomy_handler_argument_term_node_tid_depth',
      'accept depth modifier' => TRUE,
    ),
  );
}

/**
 * Implements of hook_views_handlers().
 */
function views_summary_taxonomy_views_handlers() {
  return array(
    'handlers' => array(
      'views_summary_taxonomy_handler_argument_term_node_tid_depth' => array(
        'parent' => 'views_handler_argument',
      ),
    ),
  );
}
